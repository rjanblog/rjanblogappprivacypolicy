**Privacy Policy**



**Collection of Information**

1. We collect content, communication, and other information when you use RjanBlock, including when you sign up for an account, create or share content and communicate with others. This can include in or about the content that you provide (e.g. metadata), such as the size of a photo and your personal information that you provide, such as phone number, birthday, and gender.
2. We collect information about how you use RjanBlock, such as the types of content you view or engage with, the actions you take, and the time and duration of your activities. For example, we log when you have last used Blockdit, and what posts, videos, podcasts, and other content you view on Blockdit. If you make purchases or financial transactions on Blockdit, we collect information about the purchase or transaction.
3. We collect information from and about devices you use that integrate with RjanBlock. This includes information such as the operating system, hardware and software versions, unique identifiers, device IDs. We also collect data from device settings through which you allow us to receive, such as camera or photos.


**Use of Information**

1. We use the information we have to create a personalized product that is unique and relevant to you.
2. We use the information we have to develop, test, and improve RjanBlock.
3. We use the information we have to verify accounts and activity, combat harmful conduct, detect and prevent spam and other bad experiences, maintain the integrity of RjanBlock and promote safety and security on RjanBlock.
4. We use the information we have to enforce our Terms of Service and other usage policies.


**Management of Information**

1. We store data until it is no longer necessary to provide our services or until your account is deleted - whichever comes first. This is a case-by-case determination that depends on things such as the nature of the data, why it is collected and processed, and relevant legal or operational retention needs. For example, when you search for something on RjanBlock, you can access and delete that query from within your search history at any time, but the log of that search is deleted after three months.


**RjanBlock Policy**

RjanBlock welcomes all content from people with different areas of expertise. However, posts as following are not allowed to be on Blockdit Feed.

1. Misleading or False Content
2. Offensive content towards an individual or group of individuals
3. Hate speech content towards an individual or group of individuals
4. Violence and crime-related content
5. Adult content
6. Content with exaggerating details
7. Content that persuades about multi-level marketing
8. Gambling-related content
9. Superstition-related Content
10. Copyrighted Content

Should you see any inappropriate posts that are noncompliant with Blockdit Policy, please report by clicking on the button on the upper right of the post.

RjanBlock community value User’s safety and privacy the most.

1. RjanBlock has no friend system. User is all independent.
2. Followers are unknown to other users except for those who are followed.
3. Posts that User reacts to are unknown to others except for the owner of that post.
4. People and Page Owner can ban People, Page Owner or Page from engaging with the posts.
5. On the Explore tab, User can hide posts from People or Page one doesn’t want to see.
6. You can add new credit cards or remove existing credit cards within Blockdit.


**Response to Legal Requests**

We access, preserve and share your information with regulators, law enforcements or others:

1. In response to a legal request (e.g. search warrant, court order or subpoena) if we have a good-faith belief that the law requires us to do so.
2. When we have a good-faith belief that it is necessary to: detect, prevent and address fraud, unauthorized use of RjanBlock, breaches of our Terms or Policies, or other harmful or illegal activity; to protect ourselves (including our rights, property and product), you or others, including as part of investigations or regulatory enquiries.


Information we receive about you can be accessed and preserved for an extended period when it is the subject of a legal request or obligation, governmental investigation or investigations of possible violation of our Terms or Policies, or otherwise to prevent harm. We also retain information from accounts disabled for term breaches for at least a year to prevent repeat abuse or other term breaches.


**Contact Us**

Should you have any inquiries, please do not hesitate to send an email at bank1913@gmail.com.


